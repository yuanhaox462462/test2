# -*- coding:utf-8 -*-

import requests
import argparse
from os import path
import json
import subprocess
import shlex
from urllib.request import urlopen
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class GitLabAPI(object):
    def __init__(self, headers=None, *args, **kwargs):
        self.headers = headers

    def get_user_id(self):
        username = None
        res = requests.get("https://gitlab.com/api/v4/projects/19195296/users?username=%s"%username, headers=self.headers, verify=False)
        status_code = res.status_code
        if status_code != 200:
            print('网页加载错误')
            return
        content = res.json()
        #print('content:', content)
        if content:
            username = content[0].get('username')

            #print('username:', username)
        return username

    def get_user_projects(self):
        res = requests.get("https://gitlab.com/api/v4/projects/19195296", headers=self.headers, verify=False)
        status_code = res.status_code
        if status_code != 200:
            print('网页加载错误')
            return
        content = res.json()
        #print('res:', res)
        #projects = urlopen(f'https://gitlab.com/yuanhaox462462/test3/api/v4/projects?membership=1&order_by=path&per_page=100&private_token=eKQ1PnYfiXia68M8jf-G')
        #contents = json.loads(projects.read().decode())
        #content = res.json()
        #print('contents:', contents)
        print('content:', content)
        return content

    def get_user_project_id(self, name):
        
        project_id = None
        project_name = None
        project_create_date = None
        projects = self.get_user_projects()
        #print('projects:', projects)
        if projects:
            #for item in projects:
                #if item.get('name') == name:
                    #project_id = item.get('id')
                    #print('content，project_id:', project_id)
            project_id= list(projects.values())[0]
            project_name = list(projects.values())[2]
            project_create_date = list(projects.values())[6]
            web_url = list(projects.values())[11]
            readme_url = list(projects.values())[12]
            # todo extract info from read me later on

            #project_id = projects[0].get('id')
            #print('project_id:', project_id)
        return project_id, project_name, project_create_date, web_url, readme_url

    def get_project_branchs(self, project_id):
        branchs = []
        res = requests.get("https://gitlab.com/api/v4/projects/19195296/%s/repository/branches"%project_id, headers=self.headers, verify=False)
        status_code = res.status_code
        if status_code != 200:
            print('网页加载错误')
            return
        #content = res.json()
        if content:
            for item in content:
                branchs.append(item.get('name'))
        return branchs

    def get_project_tags(self, project_id):
        tags = []
        res = requests.get("https://gitlab.com/api/v4/projects/%s/repository/tags" % project_id,
                           headers=self.headers, verify=False)
        status_code = res.status_code
        if status_code != 200:
            print('网页加载错误')
            return
        content = res.json()
        print('content:', content)
        if content:
            for item in content:
                tag_name = item.get('name')
                commit = item.get('commit')
                info = ''
                if commit:
                    commit_id = commit.get('id')
                    commit_info = commit.get('message')
                    info = "%s * %s"%(commit_id[:9], commit_info)
                tags.append("%s     %s"%(tag_name, info))
        return tags


if __name__ == "__main__":
    headers = {'PRIVATE-TOKEN': 'eKQ1PnYfiXia68M8jf-G'} #你的gitlab账户的private token
    api = GitLabAPI(headers=headers)
    content = api.get_user_projects()

    username = api.get_user_id()
    print("username:", username)

    project_id = api.get_user_project_id('project1')[0]
    project_name = api.get_user_project_id('project1')[1]
    project_create_date = api.get_user_project_id('project1')[2]
    web_url = api.get_user_project_id('project1')[3]
    readme_url = api.get_user_project_id('project1')[4]

    print("project id:: %d \nproject name: %s \ncreate date: %s" % ( project_id, project_name, project_create_date))
    print("web url: %s \nreadme_url: %s" % (web_url, readme_url))
    #branchs = api.get_project_branchs('345')
    #print("project branchs:", branchs)

    tags = api.get_project_tags(project_id)
    print("project tags:", tags)
#json.decoder.JSONDecodeError: Expecting value: line 1 column 1 (char 0) when you have an http error code like 404 and try to parse the response as JSON !