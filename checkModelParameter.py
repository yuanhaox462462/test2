import os
import tensorflow as tf
import numpy as np


checkpoint_path = os.path.join('/Users/yuanhao/ING/testtest/test2', "model.ckpt")
print("checkpoint_path: ", checkpoint_path)
# Read data from checkpoint file
reader = tf.train.load_checkpoint(checkpoint_path)
#reader = pywrap_tensorflow.NewCheckpointReader(checkpoint_path)
"""var_to_shape_map = reader.get_variable_to_shape_map()
# Print tensor name and values
for key in var_to_shape_map:
    print("tensor_name: ", key)
    print(reader.get_tensor(key))"""

var_to_shape_map = reader.get_variable_to_shape_map()
var_to_dtype_map = reader.get_variable_to_dtype_map()

var_names = sorted(var_to_shape_map.keys())
for key in var_names:
    var = reader.get_tensor(key)
    shape = var_to_shape_map[key]
    # dtype = var_to_dtype_map[key]
    # print(key, shape, dtype)
    print('key:', key,'shape:', shape,'mean:',  np.mean(var), 'variance:', np.var(var))
    #print('var:', var)