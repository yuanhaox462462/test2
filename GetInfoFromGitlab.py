#!/usr/bin/env python3
import io
import pickle
import requests
import tensorflow as tf
import numpy as np
import sys, os, urllib3, argparse, pdb
import re
from collections import defaultdict
import pandas as pd
import model
#def create_object(model, classname):   # modulename and classname are strings
   # mod = model
  #  cls = getattr(mod,classname)
 #   inst = cls()
 #   return inst


from pathlib import Path
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Add the location of python-gitlab to the path so we can import it
#repo_top = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))
#print('__file__........................................................',__file__)
#print('os.path.realpath(__file__)......................................',os.path.realpath(__file__))
#print('os.path.dirname(os.path.realpath(__file__)).....................',os.path.dirname(os.path.realpath(__file__)))
#print('os.path.join(os.path.dirname(os.path.realpath(__file__)),..)....',os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))
#print('repo_top........',repo_top)

import gitlab

def getArgs():

    parser = argparse.ArgumentParser(description='gitlab project info extraction',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("token", type=str,default="eKQ1PnYfiXia68M8jf-G", help='token')
    parser.add_argument("project_name", type=str, default="yuanhaox462462/test2",help='username/projectname')
    parser.add_argument("--url", default='https://gitlab.com/', help='https://gitlab.com/')
    parser.add_argument("model", default='model.ckpt', help='The model you would like to downloads and check')

    return parser, parser.parse_args()

class GetInfoFromGitlab():
    def __init__(self, url, token, project_name, model):
        # Parse command line arguments
        self.url = url
        self.token = token
        self.model = model
        server = gitlab.Gitlab(self.url, myargs.token, api_version=4, ssl_verify=False)
        self.project = server.projects.get(project_name)
        self.user = server.users.list()
        self.contributors= self.project.repository_contributors()
        #self.id = server.id.list()


    def IfContainContentNormalWords(self, file, check_list):
        project = self.project
        file1= file
        f = project.files.get(file_path=file1, ref='master')
        keyword=None
        # 第一次decode获得bytes格式的内容
        content = f.decode()
        # # 第二次decode获得str
        #content = content.decode()
        # 存到本地：如果本地存在会自动替换，用‘wb’，感觉直接用bytes写进去安全一点。

        with open("IfContainContent.txt", 'wb') as code:
            code.write(content)
        f1 = open("IfContainContent.txt", 'r')
        lines = f1.readlines()
        for line in lines:
            for keywords in check_list:
                if keywords in line:
                    keyword=keywords

        f1.close()
        return keyword

        #todo 上传100KB以下的文件没有问题，上传184KB大小的文件就开始报500 Internal Server Error

    def IfContainContentSpecialStr(self, file,expression):
        project = self.project
        file1= file
        f = project.files.get(file_path=file1, ref='master')
        keyword=None
        # 第一次decode获得bytes格式的内容
        content = f.decode()
        # # 第二次decode获得str
        #content = content.decode()
        # 存到本地：如果本地存在会自动替换，用‘wb’，感觉直接用bytes写进去安全一点。
        with open("IfContainContent.txt", 'wb') as code:
            code.write(content)
        f1 = open("IfContainContent.txt", 'r')
        #expression="(@[\s\S]*\}\n\})"
        ret2 = re.findall(expression, f1.read())



        #ret3 = re.search("(?<=@)[\s\S]*}}", str(ret2[0]))

        f1.close()
        return str(ret2[0])
    def data_and_algorithm_info(self):
        file1 = os.popen('git ls-tree -r --name-only HEAD').read().split('\n')
        file = open("MyFile.txt", "a")
        for i in range(len(file1)):
            if file1[i]:
                if "training.py" in file1[i]:
                    TrainingSouceCodeFile = open('training.py')

                    for eachLine in TrainingSouceCodeFile:
                        if ('mode' in eachLine) and ('import' in eachLine):
                            print("The imported model is %s \n"% eachLine)
                            file.write("The imported model is %s\n"% eachLine)
                        if ("csv.reader" in eachLine):
                            UsePythonStandardLibraryToLoadDataset=True
                        elif ("loadtxt(" in eachLine ):
                            UseNumpyToLoadDataset = True
                        elif (".read_csv(" in eachLine):
                            UsePandaToLoadDataset=True
                        #todo double check the
                        if (r"http://" in eachLine):
                            (a, afterHTTP) = eachLine.split('http')
                            (beforeCSV, d)=afterHTTP.split('.csv')
                            datasetLink='http'+beforeCSV+'.csv'
                            #file.write(os.path.split(datasetLink))
                            file.write("The datasets is %s can be found by: %s\n" % (os.path.split(datasetLink)[1],datasetLink,))
                            print("The datasets is %s can be found by: %s\n" % (os.path.split(datasetLink)[1],datasetLink,))
                            df = pd.read_csv(datasetLink, sep=';')
                            print(df.info())

                            buffer = io.StringIO()
                            df.info(buf=buffer)
                            s = buffer.getvalue()
                            with open("MyFile.txt", "a",
                                      encoding="utf-8") as f:
                                f.write(s)
                            file.write("The datasets is %s" % str(df.info()))
                            print(df.describe(include='all'))
                            file.write(str(df.describe(include='all')))
                            DataIsOnline=True

                        #todo：when created locally
                        if('.csv' in eachLine) and (r"http://" not in eachLine):
                            (beforeCSVLocally,z)=eachLine.split('.csv')
                            if(r"'" in eachLine):
                                (x,after)=beforeCSVLocally.split(r"'")

                            else:
                                (x,after)=beforeCSVLocally.split(r'"')
                            datasetLocation = after+'.csv'
                            file.write("The datasets can be found by: %s\n" % datasetLocation)
                            df = pd.read_csv(datasetLocation)
                            file.write("The dataset contains: %s\n" % df.dtypes)
                            DataSavedLocally=True

    def analysis_source_code(self):
        file1 = os.popen('git ls-tree -r --name-only HEAD').read().split('\n')
        functionCount=0
        functionNameList=[]
        file = open("MyFile.txt", "a")
        for i in range(len(file1)):
            if file1[i]:
                if "model.py" in file1[i]:
                    modelSouceCodeFile=open('model.py')
                    for eachLine in modelSouceCodeFile:
                        if ('def' in eachLine) and (eachLine.startswith('def')):
                            (beforeDef,afterDef)=eachLine.split('def')

                            (functionName,afterBrace)=afterDef.split('(')
                            functionCount+=1
                            functionNameList.append(functionName)
        file.write("There are %s functions in the model.py: %s\n" % (functionCount,functionNameList))
        print("There are %s functions in the model.py: %s\n" % (functionCount, functionNameList))

        for eachFunction in functionNameList:

            str1='model.'+eachFunction.replace(" ","")+'.__doc__'
            file.write("........................................c")
            file.write(eachFunction.replace(" ",""))

            file.write(eval(str1))



    def extract_model_info_from_mlflow(self):
        file1 = os.popen('git ls-tree -r --name-only HEAD').read().split('\n')
        project = self.project

        dict_all_params_list = []
        dict_all_params = {}
        dict_all_metrics_list = []
        dict_all_metrics = {}
        parentPathList=[]
        params_count=0
        metric_count=0
        file = open("MyFile.txt", "a")
        for i in range(len(file1)):
            if file1[i]:
                if "model.pkl" in file1[i]:
                    model_path=file1[i]
                    f = project.files.get(file_path=model_path, ref='master')
                    file_content = f.decode()
                    with open("pklFile.txt", 'wb') as code:
                        code.write(file_content)
                    f1 = open("pklFile.txt", 'rb')
                    pklFile = pickle.load(f1)
                    parentPath = str(Path(file1[i]).parent.parent.parent)
                    parentName = parentPath.split('/')[-1]
                    print("For the training %s the model is %s \n" % (parentName,str(pklFile)))
                    file.write("For the training %s the model is %s \n" % (parentName,str(pklFile)))


    def extract_info_from_mlflow(self):
        file1 = os.popen('git ls-tree -r --name-only HEAD').read().split('\n')
        project = self.project

        dict_all_params_list = []
        dict_all_params = {}
        dict_all_metrics_list = []
        dict_all_metrics = {}
        parentPathList=[]
        params_count=0
        metric_count=0
        file = open("MyFile.txt", "a")
        for i in range(len(file1)):
            if file1[i]:
                if "params" in file1[i]:
                    dict_temp = {}
                    params_count+=1
                    dict_temp['params_count'] = str(params_count)
                    #print("params path: %s" % file1[i])
                    dict_temp['params_path']=file1[i]
                    input1 = '"' + file1[i] + '"'
                    params_file = file1[i]
                    #print("params_file path: %s" % params_file)
                    f = project.files.get(file_path=params_file, ref='master')
                    paramsName = f.file_name
                    #print("params name %s" % paramsName)
                    dict_temp['params_name'] = paramsName
                    str1=str(f.decode())
                    if str1[0]=='b':
                        #print("params value %s" % str1[1:])# prefix b
                        dict_temp['params_value'] = str1[1:]
                    else:
                        #print("params value %s" % str1)
                        dict_temp['params_value'] = str1
                    #print("parents is' %s" % Path(file1[i]).parent.parent)
                    parentPath = str(Path(file1[i]).parent.parent)
                    parentName=parentPath.split('/')[-1]
                    #print("parents is %s" % parentName)
                    dict_temp['params_parent_file'] = parentName
                    AuthorName = os.popen('git log -1 --pretty=format:"%%an" -- $"%s"' % input1).readline()
                    dict_temp['author'] = AuthorName
                    createdOn = os.popen('git log --pretty=format:"%%ad" -- $"%s" | tail -1' % input1).readline()
                    dict_temp['createdOn'] = createdOn
                    #print("author: %s" % AuthorName)
                    #print("Create date: %s, " % createdOn)
                    dict_all_params_list.append(dict_temp)
                    #print("dict_all_params_list %s" % dict_all_params_list)
                    #print("dict_parent_file %s"% dict_parent_file)
                    #print("dict_temp %s" % dict_temp)
                    dict_all_params[params_count]=dict_temp
                if "metric" in file1[i]:
                    metric_dict_temp = {}
                    metric_count += 1
                    metric_dict_temp['metric_count'] = str(metric_count)
                    # print("params path: %s" % file1[i])
                    metric_dict_temp['metric_path'] = file1[i]
                    input1 = '"' + file1[i] + '"'
                    params_file = file1[i]
                    #print("!!!!!!!!!params_file path: %s" % params_file)
                    f = project.files.get(file_path=params_file, ref='master')
                    #print("!!!!!!!!!!!!test",params_file)
                    metric_name = f.file_name
                    # print("params name %s" % paramsName)
                    metric_dict_temp['metric_name'] = metric_name
                    str2 = str(f.decode())
                    if str2[0] == 'b':
                        # print("params value %s" % str1[1:])# prefix b
                        metric_dict_temp['metric_value'] = str2[1:]
                    else:
                        metric_dict_temp['metric_value'] = str2
                    parentPath = str(Path(file1[i]).parent.parent)
                    parentPathList.append(parentPath)

                    #print("!!!!!!!!!!!!test",parentPathList)
                    parentName = parentPath.split('/')[-1]
                    metric_dict_temp['metric_parent_file'] = parentName
                    AuthorName = os.popen('git log -1 --pretty=format:"%%an" -- $"%s"' % input1).readline()
                    metric_dict_temp['author'] = AuthorName
                    createdOn = os.popen('git log --pretty=format:"%%ad" -- $"%s" | tail -1' % input1).readline()
                    metric_dict_temp['createdOn'] = createdOn
                    dict_all_metrics_list.append(metric_dict_temp)
                    dict_all_metrics[metric_count] = metric_dict_temp
        #dict_all_params_list：a list contains all the parmater dict
        #dict_all_params：a dict contains all the parmater dict
        #params_count
        #params_path
        #params_name
        #params_value
        #params_parent_file
        #author
        #createdOn
        parentPathList = set(parentPathList)
        #print(parentPathList)
        result={}
        count = 0
        sorted_list = defaultdict(list)
        trainingSet = 0
        for item in dict_all_params_list:
            sorted_list[item['params_parent_file']].append(item['params_count'])
        temlist=list(sorted_list.keys())# the name of training
        trainingSet=len(temlist)
        #print(sorted_list)
        #file.write("There are %s trainings "% trainingSet)
        #print("There are %s trainings "% trainingSet)
        train_name={}
        train={}

        c2=0
        for item in temlist:

            #print(item)
            listP = []#for every training there is a list to store the parmater dict
            count+=1
            file.write("For the training %s, there are %s paramters \n" % (item, len(sorted_list[item])))
            print("For the training %s, there are %s paramters " % (item, len(sorted_list[item])))
            train_name[count]=item
            for item1 in sorted_list[item]:
                listP.append(dict_all_params[int(item1)])#add all the params which belong to one training to the list
                #print("Parameter %s " % dict_all_params[int(item1)])

            result[item]=listP# store the list to a dict, key is the training number and value is the parameters list

        #dict_all_metrics_list
        #dict_all_metrics

        #metric_count
        #metric_path
        #metric_name
        #metric_value
        #metric_parent_file
        #author
        #createdOn

        tmp2 = defaultdict(list)
        for item in dict_all_metrics_list:
            tmp2[item['metric_parent_file']].append(item['metric_count'])
        temlist2 = list(tmp2.keys())
        for item in temlist2:
            listM = []  # for every training there is a list to store the parmater dict

            if item in temlist:

            #print(item)
                file.write("For the training %s, there are %s metrics \n" % (item, len(tmp2[item])))
                print("For the training %s, there are %s metrics " % (item, len(tmp2[item])))
                for item1 in tmp2[item]:

                    listM.append(dict_all_metrics[int(item1)])
                    #print("!!!listM%s" % listM)
                result[item]=result[item]+listM
               # file.write(result)
                #print("!!!listM%s" % result)
#todo：test
            else:
                trainingSet+=1
                print("There are %s trainings \n"% trainingSet)
                count += 1
                for item1 in tmp2[item]:

                    listM.append(dict_all_metrics[int(item1)])
                result[item]=listM
       # for item in temlist:


        file.write("There are %s trainings \n"% trainingSet)
        print("There are %s trainings "% trainingSet)
        for k, v in result.items():
            file.write(str(k) + '\n')
            for va in v:
                file.write(str(va) + '\n')
         #file.write(js)
        file.close()

    def read_model(self):

        checkpoint_for_path = self.model
        checkpoint_path = os.path.join(checkpoint_for_path)
        #todo the case if model is not in the root directory and in other type
        print("checkpoint: ", checkpoint_path)
        # Read data from checkpoint file
        reader = tf.train.load_checkpoint(checkpoint_path)
        # reader = pywrap_tensorflow.NewCheckpointReader(checkpfromoint_path)


        var_to_shape_map = reader.get_variable_to_shape_map()
        var_to_dtype_map = reader.get_variable_to_dtype_map()

        var_names = sorted(var_to_shape_map.keys())
        f_model = open("MyFile_model.txt", "w")
        f_model.write("model name: %s\n" % checkpoint_path)
        f_model.write("........................................\n")
        for key in var_names:
            var = reader.get_tensor(key)
            shape = var_to_shape_map[key]
            # dtype = var_to_dtype_map[key]
            # print(key, shape, dtype)
            # print('key:', key, 'shape:', shape, 'mean:', np.mean(var), 'variance:', np.var(var))
            # print('var:', var)
            f_model.write("key: %s, shape: %s, mean: %s, variance: %s\n" % (key, shape, np.mean(var), np.var(var)))
        f_model.close()


    def list_contributors(self):

        contributors = self.contributors
        #contributors_username = contributors[0].get('name')
        #print("contributors : %s" % contributors)
        f = open("MyFile.txt", "w")
        for i in range(len(contributors)):
            contributors_username = contributors[i].get('name')
            contributors_email = contributors[i].get('email')
            contributors_commits = contributors[i].get('commits')
            print("contributors : %s" % contributors_username)
            print("contributors email: %s" % contributors_email)
            print("contributors commits: %s" % contributors_commits)
            f.write("contributors : %s\n" % contributors_username)
            f.write("contributors email: %s\n" % contributors_email)
            f.write("contributors commits: %s\n" % contributors_commits)
            f.write("........................................\n")
        f.close()

    def citation_Info(self):
        f1 = open("MyFile.txt", "a")
        file1 = os.popen('git ls-tree -r --name-only HEAD').read().split('\n')

        for i in range(len(file1)):
            if "README" in file1[i]:
                README_file = file1[i]
                #README_list = 'r\'\"\\b@\S+{\\b\"\''
                #README_list = 'r\'@\''
                expression = "(@[\s\S]*\}\n\})"
                RM = self.IfContainContentSpecialStr(README_file,expression)
        if RM:
            f1.write("........................................\n")
            f1.write("Citation: %s \n" % RM)
            print("Citation: %s " % RM)
        else:
            f1.write("........................................\n")
            print("Citation: None ")
        #f1.write("........................................\n")
        f1.close()


    def model_Info(self):
        model = self.model
        f = open("MyFile.txt", "a")
        # os.system('git ls-tree --name-only HEAD | while read filename; do echo "$(git log -1 --pretty=format:"%ad" -- $filename) $filename" >> File.txt; done')
        file1 = os.popen('git ls-tree -r --name-only HEAD').read().split('\n')
        for i in range(len(file1)):
            if file1[i]:
                # if "README" in file1[i]:
                if model in file1[i]:
                #todo 大小写

                    input1 = '"' + file1[i] + '"'
                    f.write("                model:%s\n" % file1[i])
                    print("modelFile: %s" % file1[i])
                    AuthorName = os.popen('git log -1 --pretty=format:"%%an" -- $"%s"' % input1).readline()
                    Authoremail = os.popen('git log -1 --pretty=format:"%%ae" -- $"%s"' % input1).readline()
                    lastModifed = os.popen('git log -1 --pretty=format:"%%ad" -- $"%s"' % input1).readline()
                    createdOn = os.popen('git log --pretty=format:"%%ad" -- $"%s" | tail -1' % input1).readline()
                    f.write("Author: %s \n" % AuthorName)
                    f.write("Author's email %s \n" % Authoremail)
                    f.write("Create date: %s \n" % createdOn)
                    f.write("Last modifed on: %s \n" % lastModifed)
                    print("Author: %s" % AuthorName)
                    print("Author's email: %s. " % Authoremail)
                    print("Create date: %s, " % createdOn)
                    print("Last modifed on: %s \n" % lastModifed)
                if "LICENSE" in file1[i]:
                    LICENSE_file = file1[i]
                    #print("LICENSE_file: %s" % LICENSE_file)
                    LICENSE_list = ["GPL","BSD","MIT","Mozilla","Apache","LGPL"]
                    LIC = self.IfContainContentNormalWords(LICENSE_file, LICENSE_list)
        if LIC:
            f.write("........................................\n")
            f.write("LICENSE type: %s \n" % LIC)
            print("LICENSE type: %s " % LIC)
        else:
            f.write("........................................\n")
            print("LICENSE type: None ")


        f.write("........................................\n")
        f.close()

    def list_projectInfo(self):
        project = self.project
        model=self.model

        f = open("MyFile.txt", "a")

        project_name = project.name
        project_id = project.id
        created_at = project.created_at
        web_url = project.web_url
        readme_url = project.readme_url
        print("project : %s" % project_name)
        print("project id: %s" % project_id)
        print("created at: %s" % created_at)
        print("web_url: %s" % web_url)
        print("readme_url: %s" % readme_url)
        f.write("project : %s\n" % project_name)
        f.write("project id: %s\n" % project_id)
        f.write("created at: %s\n" % created_at)
        f.write("web_url: %s\n" % web_url)
        f.write("readme_url: %s\n" % readme_url)
        f.write("........................................\n")
        f.close()
    def getExperimentsInfo(self):
        response1 = requests.get("http://127.0.0.1:5000/api/2.0/preview/mlflow/experiments/list")
        #todo：set URL， run from git
        print("list all the experimrnts")
        print(response1.content.decode("utf-8"))
        f = open("MyFile.txt", "a")
        f.write("readme_url: %s\n" % response1.content.decode("utf-8"))
        f.close()
    def run(self):
        """if input("-------------------List the contributor info，Press enter to run, any other key to skip") == '': self.list_contributors()

        if input("-------------------List the project info，Press enter to run, any other key to skip") == '': self.list_projectInfo()

        if input("-------------------List the model info and licence，Press enter to run, any other key to skip") == '': self.model_Info()
        if input("-------------------List the citation info，Press enter to run, any other key to skip") == '': self.citation_Info()
        if input("-------------------Read and explain the model paramters from checkpoint,Press enter to run, any other key to skip: ") == '': self.read_model()
        if input("-------------------Read and explain the paramters from mlflow,Press enter to run, any other key to skip: ") == '': self.extract_info_from_mlflow()
        if input("-------------------Read and explain the model info from mlflow,Press enter to run, any other key to skip: ") == '': self.extract_model_info_from_mlflow()
        if input("-------------------analysis_source_code,Press enter to run, any other key to skip: ") == '': self.analysis_source_code()
"""
        if input("-------------------Read and explain the data info and algorithm from training.py,Press enter to run, any other key to skip: ") == '': self.data_and_algorithm_info()

    # if input("-------------------List Experiment infor,Press enter to run, any other key to skip: ") == '': self.getExperimentsInfo()
if __name__ == '__main__':
    myParser, myargs = getArgs()
    sys.exit(GetInfoFromGitlab(url=myargs.url, token=myargs.token, project_name=myargs.project_name, model=myargs.model).run())
