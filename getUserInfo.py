#!/usr/bin/env python
#-*-coding:utf-8-*-

import requests

url = 'https://gitlab.com/yuanhaox462462/hello/api/v3/projects?private_token=eKQ1PnYfiXia68M8jf-G&per_page=50'
#private_token为必须项，这里为获取所有的项目信息

user_url= 'https://gitlab.com/yuanhaox462462/hello/api/v3/projects/{}/users?private_token=eKQ1PnYfiXia68M8jf-G&per_page=100'
#获取每个项目下的用户信息
#http://10.10.10.217/api/v3/projects/45/users?private_token=oMJwN5ErC8_n1QvTsyDR　　　　　　#获取项目id为45的信息
#获取项目id和项目名称
def GetProject_id(project_url):
    r = requests.get(project_url)
    data = r.json()
    ProjectId_list = []
    ProjectName_list = []
    for i in data:
        ProjectId_list.append(i['id'])
        ProjectName_list.append(i['name'])
    return ProjectId_list,ProjectName_list
#根据项目id获取项目下的用户信息
def GetProject_userlist():
    IdList = GetProject_id(url)
    project_id = IdList[0]
    project_name = IdList[1]

    for id in project_id:
        l = []
        project_user = requests.get(user_url.format(id))
        #生成完整的用于显示项目下所有user的连接
        req_data = project_user.json()
        for i in req_data:
            l.append(i['name'])
        print (project_name[project_id.index(id)],l)

if __name__ == '__main__':
    GetProject_userlist()